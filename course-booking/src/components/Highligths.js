import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
	return(


		<Row className = "mt-3 mb-3">
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus repudiandae ipsa molestiae numquam? Non ducimus suscipit, accusantium quis, officia atque facilis laudantium sint aut quidem eos ut magnam ratione, rem?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus repudiandae ipsa molestiae numquam? Non ducimus suscipit, accusantium quis, officia atque facilis laudantium sint aut quidem eos ut magnam ratione, rem?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be part of our community.</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus repudiandae ipsa molestiae numquam? Non ducimus suscipit, accusantium quis, officia atque facilis laudantium sint aut quidem eos ut magnam ratione, rem?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>




	)
}